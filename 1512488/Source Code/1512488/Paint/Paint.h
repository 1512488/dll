#pragma once

#include "resource.h"
#include <iostream>
#include <vector>
#include <windowsx.h>
#include <windowsX.h>
#include <assert.h>
#include <comdef.h>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "comctl32.lib")

#define LINE 1
#define RECTANGLE 2
#define ELLIPSE 3

extern bool Drawing;				
extern HWND gPaintDraw;				
extern POINT pHienHanh, pKT;		
extern HPEN gPenDraw;					
extern int isOpenFile;				
extern int Pen_size;			
extern int iChoose;					
extern COLORREF iColor;	