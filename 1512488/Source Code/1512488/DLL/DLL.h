// DLL.cpp : Defines the exported functions for the DLL application.
//
#pragma once

#define PAINTLIBLIBRARY_API _declspec(dllexport)

#include <windowsX.h>
#include "stdafx.h"
using namespace std;
namespace PaintLibrary
{
	class PAINTLIBLIBRARY_API CShape
	{
	protected:
		POINT a, b;
		COLORREF sColor;
		int sSize;
	public:
		virtual void Draw(HDC hdc) = 0;
		virtual CShape* Create() = 0;
		virtual void SetData(int a, int b, int c, int d, COLORREF clr, int sz) = 0;
		CShape();
		~CShape();
	};
}